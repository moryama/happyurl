import datetime


def log_this(page_checkup: list) -> None:
    """Log the health check and the url of each link in a ``latest_checkup.txt`` file.

    :param page_checkup: list of namedtuples
    """

    curr_time = datetime.datetime.now()

    with open('latest_checkup.txt', 'w') as log:

        log.write(f'{curr_time}\n')

        for link in page_checkup:

            if link == 'No links found':
                log.write('I found no links on the page')
                return

            if not link.url:
                log.write(f'==== INVALID URL ===> {link.url}\n')
                continue

            log.write(f'======== {link.response} =======> {link.url}\n')

    return None
