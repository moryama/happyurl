from typing import Optional
from urllib.parse import urlparse


class Url():

    def __init__(self, url: str):
        self.url = url
    
    def validate(self) -> Optional[str]:
        """Return the url, if valid, or return None."""

        if self.url is None:
            return None

        parsed_url = urlparse(self.url)

        if not parsed_url.scheme or not parsed_url.netloc:
            return None
        else:
            return self.url


def response_is_ok(r) -> bool:
    
    if 400 <= r.status_code < 500 or \
       500 <= r.status_code < 600:
        return False
    else:
        return True
