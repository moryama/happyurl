from collections import namedtuple
import sys
from typing import NamedTuple

import requests
from requests.exceptions import HTTPError, InvalidURL
from requests_html import HTMLSession

from .logging import log_this
from .helpers import Url, response_is_ok


def get_links(page_url: str) -> list:
    """Collect clickable links on the page at the given url, sans email addresses.

    :param page_url: url of the page
    :type page_url: string
    :return: list of all links living on the page
    :rtype: list of strings
    """

    session = HTMLSession()
    r = session.get(page_url)

    if not response_is_ok(r):
        raise HTTPError(f'Could not reach {page_url}')

    links = list(r.html.links)  # type: ignore

    return links


def check_all_urls(urls: list) -> list:
    """Run the check_url() function on each url in the list of urls.

    :param url: list of url
    :type urls: list[str]
    :return: list of namedtuples UrlCheckup
    :rtype: list[NamedTuple]
    """

    urls_checkup = []
    for url in urls:
        urls_checkup.append(check_url(url))
    return urls_checkup


def check_url(url: str) -> NamedTuple:
    """Check the url passed is a valid url and 
    store its response's status code in a dictionary.

    :param url: url to be checked
    :type url: str
    :return: namedtuple wiht fields [url, response], defaults=None
    :rtype: namedtuple UrlCheckup
    """

    UrlCheckup = namedtuple('UrlCheckup', (
                            'url', 'response',),
                            defaults=(None, None,)
                            )

    valid_url = Url(url).validate()

    if not valid_url:
        return UrlCheckup()
    else:
        r = requests.head(url, timeout=15)  # Slow showcase-sites are real
        url_checkup = UrlCheckup(url=url,
                                 response=r.status_code)

    return url_checkup


def run_checkup(page_url: str) -> bool:
    """Check the link passed is a valid url then
    run all the functions that constitute the checkup and
    finally log the checkup data.

    :param page_url: url of the page to be checked
    :type page_url: str
    """

    valid_url = Url(page_url).validate()

    if not valid_url:
        raise InvalidURL(f'This is not a valid url: {page_url}')

    links_in_page = get_links(page_url)

    if not links_in_page:
        log_this(['No links found'])
        return False

    page_checkup = check_all_urls(links_in_page)

    if not page_checkup:
        log_this(['No health check available'])
        return False

    log_this(page_checkup)

    return True


def get_page_url():
    """Return the url of the page to be checked.

    :return: user defined url
    :rtype: str
    """

    from .user.page_url import page_url
    return page_url


def run():
    """Run a checkup on a given page and print a feedback in the terminal.

    :param page_url: url of the page to be checked
    :type page_url: str
    """

    page_url = get_page_url()

    if not page_url:
        print('Please provide the url of the page to check.')
        print('To do so, modify file src/user/page_url.py')
        sys.exit()

    checkup = run_checkup(page_url)

    if not checkup:
        print('Sorry, I could not complete the checkup.')
    else:
        print('Checkup successfully completed.')

    print('Please see latest_checkup.txt for more info.')

    return
