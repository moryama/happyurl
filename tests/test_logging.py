import datetime

from src.logging import log_this
from tests.conftest import CHECKUP_DATA, EXPECTED_LOG_DATA


class TestLogThis():

    def test_should_log_current_data(self, test_log):

        curr_time = datetime.datetime.now()

        log_this(CHECKUP_DATA)

        with open('latest_checkup.txt') as log:
            first_line = log.readline()

        assert first_line[:16] == str(curr_time)[:16]

    def test_should_log_complete_data(self, test_log):

        log_this(CHECKUP_DATA)

        with open('latest_checkup.txt') as log:
            log_data = log.read()

        assert EXPECTED_LOG_DATA in log_data
