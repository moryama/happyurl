"""A bare bones web application used for testing our program"""

import time
from flask import Flask, render_template


def create_test_app():
    """Run a basic web app with routes"""

    app = Flask(__name__)

    @app.route('/links')
    def page_with_links():
        return render_template('links.html')

    @app.route('/blank')
    def blank_page():
        return 'A blank page'

    @app.route('/404')
    def page_not_found():
        return '404 - Page not found', 404

    @app.route('/slow')
    def slow_page():
        time.sleep(5)
        return 'A slow loading page'

    @app.route('/testlink')
    def just_a_link():
        return 'Just a test link'

    @app.route('/testanotherlink')
    def just_another_link():
        return 'Just another test link'

    @app.route('/testyetanotherlink')
    def yet_another_link():
        return 'Yet another test link'

    return app
