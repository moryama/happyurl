"""
A mock server for testing using a Flask application

Source (with modifications | License: MIT) 
https://gist.github.com/eruvanos/f6f62edb368a20aaa880e12976620db8

"""


import requests

from flask import Flask, jsonify, request
from threading import Thread

from tests.testpage.app import create_test_app


TEST_APP = create_test_app()


class MockServer(Thread):
    def __init__(self, port):
        super().__init__()
        self.port = port
        self.app = TEST_APP
        self.url = 'http://localhost:%s' % self.port

        self.app.add_url_rule('/shutdown', view_func=self._shutdown_server)

    def _shutdown_server(self):

        if not 'werkzeug.server.shutdown' in request.environ:
            raise RuntimeError('Not running the development server')

        request.environ['werkzeug.server.shutdown']()
        return 'Server shutting down...'

    def shutdown_server(self):
        """Call the endpoint that shuts down the server"""

        requests.get('http://localhost:%s/shutdown' % self.port)
        self.join()

    def run(self):
        self.app.run(port=self.port)
