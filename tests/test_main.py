from urllib.parse import urljoin

import pytest
from requests.exceptions import InvalidURL
from requests.models import HTTPError

from src.main import (check_all_urls,
                      check_url,
                      get_links,
                      get_page_url,
                      run_checkup)

from tests.conftest import EXPECTED_LINKS


class TestGetLinks():

    def test_should_return_all_links_from_a_page(self, mock_server):

        page_url = urljoin(mock_server.url, '/links')

        expected = EXPECTED_LINKS

        output = get_links(page_url)

        assert isinstance(output, list)
        assert len(output) == 4
        assert sorted(output) == sorted(expected)

    def test_should_raise_HTTP_error_if_page_status_code_is_not_ok(self, mock_server):

        page_url = urljoin(mock_server.url, '/404')

        with pytest.raises(HTTPError):
            get_links(page_url)

    def test_should_return_empty_list_if_no_links_found(self, mock_server):

        page_url = urljoin(mock_server.url, '/blank')

        output = get_links(page_url)

        assert output == []


class TestCheckAllUrls():

    def test_should_return_a_list_of_length_four(self, mock_server):

        url_list = EXPECTED_LINKS

        output = check_all_urls(url_list)

        assert isinstance(output, list)
        assert len(output) == 4


class TestCheckUrl():

    def test_should_return_response_status_code(self, mock_server):

        url = urljoin(mock_server.url, '/links')

        output = check_url(url)

        assert output.url == url  # type: ignore
        assert output.response == 200  # type: ignore


class TestRunCheckup():

    def test_should_return_true_if_checkup_is_completed(self, mock_server):

        page_url = urljoin(mock_server.url, '/links')

        assert run_checkup(page_url) is True

    @pytest.mark.parametrize('page_url', (
                            ('http//127.0.0.1:5000/'),
                            (None),
                            ('')))
    def test_should_raise_exception_if_URL_INVALID_is_passed(self, page_url):

        page_url = page_url

        with pytest.raises(InvalidURL) as e:
            run_checkup(page_url)

        assert str(e.value) == f'This is not a valid url: {page_url}'


class TestGetPageUrl():

    def test_should_return_file_content(self):

        output = get_page_url()
        expected = ''

        assert output == expected
