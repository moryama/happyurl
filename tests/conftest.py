import os
from collections import namedtuple

import pytest

from tests.mockserver import MockServer
from tests.testpage.app import create_test_app

"""Mock server fixture"""


@pytest.fixture(scope='session')
def mock_server(request):

    server = MockServer(port=5000)
    server.start()

    yield server

    server.shutdown_server()


"""Test app fixture"""


@pytest.fixture
def app():
    app = create_test_app()
    with app.app_context():
        yield app


@pytest.fixture
def client(app):
    return app.test_client()


"""Test data"""

URL = 'http://127.0.0.1:5000'

EXPECTED_LINKS = [f'{URL}/slow',
                  f'{URL}/testlink',
                  f'{URL}/testanotherlink',
                  f'{URL}/testyetanotherlink'
                  ]

TestUrlCheckup = namedtuple('TestUrlCheckup', (
                            'url', 'response',),
                            defaults=(None, None,)
                            )

CHECKUP_DATA = [TestUrlCheckup(
    url=f'{URL}/slow',
    response=200),
    TestUrlCheckup(
    url=f'{URL}/testlink',
    response=200),
    TestUrlCheckup(
    url=f'{URL}/testanotherlink',
    response=200),
    TestUrlCheckup(
    url=f'{URL}/testyetanotherlink',
    response=200)]

{f'{URL}/slow': {'response': 200},
 f'{URL}/testlink': {'response': 200},
 f'{URL}/testanotherlink': {'response': 200},
 f'{URL}/testyetanotherlink': {'response': 200}}

EXPECTED_LOG_DATA = f"""======== 200 =======> {URL}/slow\n======== 200 =======> {URL}/testlink\n======== 200 =======> {URL}/testanotherlink\n======== 200 =======> {URL}/testyetanotherlink\n"""


"""Test log fixture"""


@pytest.fixture(scope='session')
def test_log():

    test_log = open('latest_checkup.txt', 'w')

    yield

    test_log.close()
    os.remove('latest_checkup.txt')
