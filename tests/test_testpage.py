from urllib.parse import urljoin


def test_testpage_response(client, mock_server):

    url = urljoin(mock_server.url, '/links')

    r_links = client.get(url)

    assert r_links.status_code == 200
