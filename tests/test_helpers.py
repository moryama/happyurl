import pytest
import requests

from src.helpers import Url, response_is_ok


VALID_URL = 'http://127.0.0.1:5000/links'
INVALID_URL = 'http//127.0.0.1:5000/'


class TestUrlClass():

    @pytest.mark.parametrize(('url', 'expected'),
                             ((VALID_URL, VALID_URL),
                              (INVALID_URL, None),
                              (None, None),
                              ('', None)
                              ))
    def test_validate_method(self, url, expected):

        output = Url(url).validate()

        assert output == expected


class TestResponseIsOk():

    def test_check_url_with_response_ok(self):

        r = requests.Response()
        r.status_code = 200

        assert response_is_ok(r)

    def test_check_url_with_response_404(self):

        r = requests.Response()
        r.status_code = 404

        assert not response_is_ok(r)
