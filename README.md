# HappyUrl 

<img src="./docs/_static/logo.png" alt="picture of a happy link" align="right" width="200"/>

I wrote this script to **check the links** on my personal web page and make sure they work fine.

I can use the script by connecting it to a regular event on my calendar to automatically run a checkup.

## Features:

- **parses** a given webpage and **collects** all external links
- checks the **status code** of each link's url
- **logs** the results in a ``.txt`` file
- comes with a Sphynx [documentation](https://happyurl.readthedocs.io/en/latest/)
- has **unit** and **integration tests**

## Tools

- [Python 3.7](https://docs.python.org/3.7/)
- [Requests](https://requests.readthedocs.io/en/master/)
- [Pytest](https://docs.pytest.org/en/latest/contents.html)
- [Flask](https://flask.palletsprojects.com/en/1.1.x/) to build a mock application used for testing
- [Sphynx](https://www.sphinx-doc.org/en/master/) to generate the documentation

## TODOs and beyond
- implement an email notification that notifies about any broken links
- apart from checking link status, check that it takes to a relevant page

## Usage

Install:
```
$ git clone https://gitlab.com/moryama/happyurl.git
$ cd happyurl
$ pip install -r requirements.txt
```

Customize:
```
# happyurl/src/user/page_url.py

page_url = 'https://www.mypersonalpage.com' # the page you want to parse
```

Run:
```
$ python run.py
```

After running is completed, check out the ``happyurl/latest_checkup.txt`` file for your log. 

Hopefully all your links are happy. 🐸

## License

No license is available at the moment.