.. _install-and-run:

Install and Run
===============

Quick guide
-----------
::

    $ git clone https://gitlab.com/moryama/happyurl.git
::

    $ pip install -r requirements.txt
Go to ``happyurl/src/user/page_url.py``::

    # INSTRUCTIONS:
    # Fill in '' with the url of the page you want to check
    page_url = 'https://www.mypersonalpage.com'
::

    $ python run.py
Check out ``happyurl/latest_checkup.txt``.

Step by step guide 
------------------

This is a step by step guide to install, set up and run HappyUrl.

- First, make sure you have HappyUrl on your machine. Open your terminal and navigate to a place where you want the project to live. Then type this in your terminal::

    $ git clone https://gitlab.com/moryama/happyurl.git

- Next, navigate to the ``happyurl`` directory by typing this::

    $ cd happyurl

- At this point, it is recommended that you create and activate a `virtual environment <https://realpython.com/python-virtual-environments-a-primer/#using-virtual-environments>`_.

- Once you have activated your virtual environment, install the program's dependencies. Type this in you terminal::

    $ pip install -r requirements.txt

- In your editor of choice, navigate the project to ``happyurl/src/user/`` and open the file ``page_url.py``. Change this file by filling in the url of the page you want to check. Like in this example::

    # INSTRUCIONS:
    # Fill in '' with the url of the page you want to check
    page_url = 'https://www.mypersonalpage.com'

- Save and close the file.

- Finally, go back to the terminal. Make sure you are in the ``happyurl`` directory. 

- Type the following in your terminal::

    $ python run.py

- Inside your ``happyurl`` you'll find a ``latest_checkup.txt`` file. Open it.

May your urls be as happy as they can be!