Welcome to HappyUrl
===================================

You have a web page you care about - like your portfolio or a documentation page. From time to time, you want to make sure the links on your page are still working fine. This command-line program will help you do exactly that.

**What HappyUrl does:**

* collects all the external links on your page
* checks the status code of each link
* logs the results in a ``.txt`` file

`Project source code <https://www.moryama.gitlab.io/happyurl>`_

User Guide
----------

.. toctree::
   :maxdepth: 2

   user/install-and-run

Technical Guide
---------------

.. toctree::
   :maxdepth: 2

   tech/source-code
   tech/tests
